CREATE DATABASE establecimientos;

USE establecimientos;

CREATE TABLE establecimientos (intIdEstablecimiento int(10) NOT NULL AUTO_INCREMENT, strGiro varchar(100), strSubgiro varchar(100), strNombreEstablecimiento varchar(255), strHoraCierre varchar(255), strHoraApertura varchar(255), strLatitud varchar(100), strLongitud varchar(100), strImagen varchar(9999), strUrlPagina varchar(500), strCorreo varchar(100), PRIMARY KEY (intIdEstablecimiento));
