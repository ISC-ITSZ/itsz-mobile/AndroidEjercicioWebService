package com.isc.itsz.chema.ejerciciowebservice;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.Permissions;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, LocationListener, View.OnClickListener {

    private Spinner spinGiros;
    private Spinner spinSubGiros;
    ArrayAdapter spinAdapterSubgiro;
    public static String strLat, strLong;
    private LocationManager locationManager;
    String strGiroSeleccionado = "";

    public EditText etNombreEstablecimiento;
    public EditText etHoraApertura;
    public EditText etHoraCierre;
    public EditText etTelefono;
    public EditText etUrlPagina;
    public EditText etCorreo;
    ///String SERVER = "";
    String OBTENER_ESTABLECIMIENTOS = "http://192.168.1.92/ObtenerEstablecimientos";
    String REGISTRAR_ESTABLECIMIENTO = "http://lunacy.000webhostapp.com/api_803/RegistrarEstablecimiento";

    private ProgressDialog pDialog;
    private JSONObject json;
    private int success = 0;
    private HTTPURLConnection service;


    public static String latitud, longitud;
    public String strGiro,
            strSubgiro,
            strCorreo,
            strUrlPagina,
            strTelefono,
            strNombreEstablecimiento,
            strHoraApertura,
            strHoraCierre,
            strLatitud,
            strLongitud,
            strImagen;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        spinGiros = (Spinner) findViewById(R.id.spinGiros);
        spinSubGiros = (Spinner) findViewById(R.id.spinSubGiros);

        etNombreEstablecimiento = (EditText)findViewById(R.id.etNombreEstablecimiento);
        etHoraApertura = (EditText)findViewById(R.id.etHoraApertura);
        etHoraCierre= (EditText)findViewById(R.id.etHoraCierre);
        etTelefono= (EditText)findViewById(R.id.etTelefono);
        etUrlPagina= (EditText)findViewById(R.id.etUrlPagina);
        etCorreo= (EditText)findViewById(R.id.etCorreo);

        strImagen = "";
        Button btnGetCoor = (Button) findViewById(R.id.btnGetCoor);
        Button btnEnviarDatos = (Button)findViewById(R.id.btnEnviarDatos);

        service = new HTTPURLConnection();
        btnGetCoor.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obtenerCoordenadas();
            }
        });

        btnEnviarDatos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                guardarDatos();

            }
        });

        ArrayAdapter spinAdapterGiro = ArrayAdapter.createFromResource(this, R.array.saGiros, android.R.layout.simple_spinner_item);
        //= ArrayAdapter.createFromResource(this, R.array.saSubGiroComercial, android.R.layout.simple_spinner_item);
        spinGiros.setAdapter(spinAdapterGiro);

        spinGiros.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {//listado, posición, ?
                strGiro = adapterView.getItemAtPosition(i).toString();

                if (adapterView.getItemAtPosition(i).equals("Industrial")) {
                    spinAdapterSubgiro = ArrayAdapter.createFromResource(MainActivity.this, R.array.saSubGiroIndustrial, android.R.layout.simple_spinner_item);
                    spinSubGiros.setAdapter(spinAdapterSubgiro);
                }
                if (adapterView.getItemAtPosition(i).equals("Comercial")) {
                    spinAdapterSubgiro = ArrayAdapter.createFromResource(MainActivity.this, R.array.saSubGiroComercial, android.R.layout.simple_spinner_item);
                    spinSubGiros.setAdapter(spinAdapterSubgiro);
                }
                if (adapterView.getItemAtPosition(i).equals("Servicios")) {
                    spinAdapterSubgiro = ArrayAdapter.createFromResource(MainActivity.this, R.array.saSubGiroServicios, android.R.layout.simple_spinner_item);
                    spinSubGiros.setAdapter(spinAdapterSubgiro);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinSubGiros.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strSubgiro = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        */
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } /*else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onClick(View view) {

    }

    public boolean obtenerDatosFormulario(){
        boolean result = false;


        strNombreEstablecimiento = etNombreEstablecimiento.getText().toString();
        strHoraApertura = etHoraApertura.getText().toString();
        strHoraCierre = etHoraCierre.getText().toString();
        strTelefono = etTelefono.getText().toString();
        strUrlPagina = etUrlPagina.getText().toString();
        strCorreo = etCorreo.getText().toString();

        if(strLongitud == null)
            return false;
        if(strLatitud.length() > 0 &&
                strLongitud.length() > 0 &&
                strNombreEstablecimiento.length() > 0 &&
                strHoraApertura.length() > 0 &&
                strHoraCierre.length() > 0 &&
                strTelefono.length() > 0 &&
                strUrlPagina.length() > 0
                && strCorreo.length() > 0 &&
                strGiro.length() > 0 &&
                strSubgiro.length() > 0)
                result = true;


        return result;
    }

    public void guardarDatos(){
        if(obtenerDatosFormulario()){
            new PostDataTOServer().execute();

                  /*  "{\n" +
                            "\"strGiro\":\" " + strGiro + " \",\n" +
                            "\"strSubgiro\":\" " + strSubgiro + " \",\n" +
                            "\"strNombreEstablecimiento\":\" " + strNombreEstablecimiento + " \",\n" +
                            "\"strHoraApertura\":\" " + strHoraApertura + " \",\n" +
                            "\"strHoraCierre\":\" " + strHoraCierre + " \",\n" +
                            "\"strLatitud\":\" " + strLatitud + " \",\n" +
                            "\"strLongitud\":\" " + strLongitud + " \",\n" +
                            "\"strImagen\":\"  " + strImagen + "  \",\n" +
                            "\"strUrlPagina\":\"  " + strUrlPagina + "  \",\n" +
                            "\t\"strCorreo\":\" " + strCorreo + " \"\n" +
                            "}");   /*/ //Parámetros que recibe doInBackground
        }
        else
            Toast.makeText(MainActivity.this, "DATOS INCOMPLETOS", Toast.LENGTH_LONG).show();
    }

    public void obtenerCoordenadas() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        getLocation();
    }

    void getLocation(){
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                                                PackageManager.PERMISSION_GRANTED &&
                                                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                                                PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
        else{
            Location location = locationManager.getLastKnownLocation(locationManager.NETWORK_PROVIDER);
            if(location != null){
                double latti = location.getLatitude();
                double longit = location.getLongitude();

                strLatitud = Double.toString(latti);
                strLongitud = Double.toString(longit);
                Toast.makeText(MainActivity.this, latti + "," + longit, Toast.LENGTH_LONG).show();
            }

            //onLocationChanged(location);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,@NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1
                    :
                getLocation();
                break;
        }
    }

    ///////////////////////////////////////////////////////////777
    private class PostDataTOServer extends AsyncTask<Void, Void, Void> {

        String response = "";
        //Create hashmap Object to send parameters to web service
        HashMap<String, String> postDataParams;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected Void doInBackground(Void... arg0) {

            /*"\"strGiro\":\" " + strGiro + " \",\n" +
                    "\"strSubgiro\":\" " + strSubgiro + " \",\n" +
                    "\"strNombreEstablecimiento\":\" " + strNombreEstablecimiento + " \",\n" +
                    "\"strHoraApertura\":\" " + strHoraApertura + " \",\n" +
                    "\"strHoraCierre\":\" " + strHoraCierre + " \",\n" +
                    "\"strLatitud\":\" " + strLatitud + " \",\n" +
                    "\"strLongitud\":\" " + strLongitud + " \",\n" +
                    "\"strImagen\":\"  " + strImagen + "  \",\n" +
                    "\"strUrlPagina\":\"  " + strUrlPagina + "  \",\n" +
                    "\t\"strCorreo\":\" " + strCorreo + " \"\n" +*/


            postDataParams=new HashMap<String, String>();
            postDataParams.put("strGiro", strGiro);
            postDataParams.put("strSubgiro", strSubgiro);
            postDataParams.put("strNombreEstablecimiento", strNombreEstablecimiento);
            postDataParams.put("strHoraApertura", strHoraApertura);
            postDataParams.put("strHoraCierre", strHoraCierre);
            postDataParams.put("strLatitud", strLatitud);
            postDataParams.put("strLongitud", strLongitud);
            postDataParams.put("strImagen", strImagen);
            postDataParams.put("strUrlPagina", strUrlPagina);
            postDataParams.put("strCorreo", strCorreo);
            response= service.ServerData(REGISTRAR_ESTABLECIMIENTO,postDataParams);
            try {
                json = new JSONObject(response);

                //Toast.makeText(getApplicationContext(), response.toString(), Toast.LENGTH_LONG).show();
                //System.out.println(response.toString());
                //Get Values from JSONobject
                //System.out.println("success=" + json.get("response"));
                //success = json.getInt("success");

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing())
                pDialog.dismiss();
            if(success==1) {
                Toast.makeText(getApplicationContext(), "DATOS REGISTRADOS..!", Toast.LENGTH_LONG).show();
            }
        }
    }




}
