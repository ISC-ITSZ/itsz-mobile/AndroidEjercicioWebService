<?php
require 'config.php';
require 'Slim/Slim.php';
require 'establecimientos.php';

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();

$app->post('/RegistrarEstablecimiento', 'registrarEstablecimiento');
$app->post('/ObtenerEstablecimientos', 'obtenerTodosEstablecimientos');

$app->run();

?>