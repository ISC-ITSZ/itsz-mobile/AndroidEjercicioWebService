<?php
function registrarEstablecimiento(){

    $request = \Slim\Slim::getInstance()->request();
    $data = json_decode($request->getBody());

    
    try{
        /*if(!isset($data)){
                echo '{"statusCode":"500", "response":[{"error":"No se recibieron datos"}]}';
                return;
        }*/


        $db = getDB();
        $sqlSentence = "INSERT INTO establecimientos(strGiro, strSubgiro, strNombreEstablecimiento, strHoraCierre, strHoraApertura, strLatitud, strLongitud, strImagen, strUrlPagina, strCorreo) VALUES (:strGiro, :strSubgiro, :strNombreEstablecimiento, :strHoraCierre, :strHoraApertura, :strLatitud, :strLongitud, :strImagen, :strUrlPagina, :strCorreo)";
        $statement = $db->prepare($sqlSentence);

        $statement->bindParam("strGiro", $data->strGiro, PDO::PARAM_STR);
        $statement->bindParam("strSubgiro", $data->strSubgiro, PDO::PARAM_STR);
        $statement->bindParam("strNombreEstablecimiento", $data->strNombreEstablecimiento, PDO::PARAM_STR);
        $statement->bindParam("strHoraApertura", $data->strHoraApertura, PDO::PARAM_STR);
        $statement->bindParam("strHoraCierre", $data->strHoraCierre, PDO::PARAM_STR);
        $statement->bindParam("strLatitud", $data->strLatitud, PDO::PARAM_STR);
        $statement->bindParam("strLongitud", $data->strLongitud, PDO::PARAM_STR);
        
        $statement->bindParam("strImagen", $data->strImagen, PDO::PARAM_STR);
        $statement->bindParam("strUrlPagina", $data->strUrlPagina, PDO::PARAM_STR);
        $statement->bindParam("strCorreo", $data->strCorreo, PDO::PARAM_STR);

        if($statement->execute() > 0 && $statement->rowCount() > 0)
            echo '{"statusCode":"200", "response":[{"text":"El establecimiento ha sido guardado"}]}';
        else
            echo '{"statusCode":"200", "response":[{"error":"La operación no se completó"}]}';
    }
    catch(PDOException $e){
        echo '{"statusCode":"500", "response":[{"error":"Ha ocurrido un error interno. '.$e->getMessage().'"}]}';
    }
}

function obtenerTodosEstablecimientos(){
    try{
        $db = getDB();
        $sqlSentence = "SELECT * FROM establecimientos";
        $statement = $db->prepare($sqlSentence);
        $statement->execute();
        $todosEst = null;
        if($statement)
            $todosEst = $statement->fetchAll(PDO::FETCH_OBJ);

        if($todosEst)
            echo '{"statusCode":"200", "response":{"data":'.json_encode($todosEst).'}}';
        else
            echo '{"statusCode":"200", "response":[{"empty":"No hay establecimientos registrados"}]}';
    }
    catch(PDOException $e){
        echo '{"statusCode":"500", "response":[{"error":"Ha ocurrido un error interno"}]}';
    }
}


?>